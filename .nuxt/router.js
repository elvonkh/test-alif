import Vue from 'vue'
import Router from 'vue-router'
import { interopDefault } from './utils'
import scrollBehavior from './router.scrollBehavior.js'

const _598b1f5e = () => interopDefault(import('..\\pages\\detail\\_contact.vue' /* webpackChunkName: "pages_detail__contact" */))
const _09165f83 = () => interopDefault(import('..\\pages\\index.vue' /* webpackChunkName: "pages_index" */))

Vue.use(Router)

export const routerOptions = {
  mode: 'history',
  base: decodeURI('/'),
  linkActiveClass: 'nuxt-link-active',
  linkExactActiveClass: 'nuxt-link-exact-active',
  scrollBehavior,

  routes: [{
    path: "/detail/:contact?",
    component: _598b1f5e,
    name: "detail-contact"
  }, {
    path: "/",
    component: _09165f83,
    name: "index"
  }],

  fallback: false
}

export function createRouter () {
  return new Router(routerOptions)
}
