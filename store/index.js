export const state = () => ({
  contacts: []
})

export const mutations = {
  addContact(state, payload) {
    state.contacts.push(payload)
    state.contacts.forEach((item, i) => (item.id = (i + 1).toString()))
  },
  deleteContact(state, contactIndex) {
    state.contacts.splice(contactIndex, 1)
  },
  updateName(state, { params, value }) {
    const currentContact = state.contacts.find((obj) => (obj.id = params))
    currentContact.name = value
  },
  // Stateless mutations
  removeItem(state, { currentArray, currentIndex }) {
    currentArray.splice(currentIndex, 1)
  },
  addField(state, { currentArray, field }) {
    currentArray.push(field)
  },
  updateValue(state, { value, field }) {
    field.value = value
  }
}

export const getters = {
  getContact({ contacts }) {
    return contacts
  }
}
